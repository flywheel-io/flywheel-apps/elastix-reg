from pathlib import Path

import pytest
from nipype import Workflow

from fw_gear_elastix_reg import workflow


def test_split_string():
    test_outputs = ["abc-def"]

    output1 = workflow.split_string(test_outputs)

    assert output1 == "abc-def"


def test_config_elastix(tmpdir):
    mock_work_dir = Path(tmpdir.mkdir("work"))

    actual_wf = workflow.config_elastix_wf(
        mock_work_dir,
        True,
    )
    assert isinstance(actual_wf, Workflow)

    assert actual_wf.base_dir == mock_work_dir

    assert actual_wf.list_node_names() == [
        "registration",
        "split_output",
        "transformix",
    ]
