import os
from pathlib import Path
from unittest.mock import MagicMock

import pytest
from nibabel.testing import data_path
from nipype import Workflow

from fw_gear_elastix_reg import interfaces, main


@pytest.fixture
def example_nifti_image():
    return os.path.join(data_path, "example4d.nii.gz")


@pytest.fixture
def example_config_json(tmpdir):
    sub = tmpdir.mkdir("sub")
    sub.join("testfile.txt").write("content")
    return {
        "inputs": {
            "elastix_config": {"location": {"path": str(sub.join("testfile.txt"))}},
            "source": {
                "location": {"path": str(os.path.join(data_path, "example4d.nii.gz"))}
            },
            "target": {
                "location": {"path": str(os.path.join(data_path, "example4d.nii.gz"))}
            },
        }
    }


def test_run(tmpdir, mocker):
    mock_config_elastix = mocker.patch("fw_gear_elastix_reg.main.config_elastix_wf")
    mock_config_elastix.return_value = MagicMock(spec=Workflow)

    mock_connect_utility_nodes = mocker.patch(
        "fw_gear_elastix_reg.main.connect_utility_nodes"
    )
    mock_connect_utility_nodes.return_value = MagicMock(spec=Workflow)

    mock_output_dir = Path(tmpdir.mkdir("output"))
    mock_work_dir = Path(tmpdir.mkdir("work"))
    mock_gear_context_interface = MagicMock(
        spec=interfaces.ElastixRegGearContextInterface
    )
    logging_mock = mocker.patch("logging.Logger.info")

    main.run(mock_gear_context_interface, mock_output_dir, mock_work_dir, True)

    logging_mock.assert_called_with("Elastix has finished successfully...")


def test_run_with_exception(tmpdir, mocker, example_config_json):
    mock_output_dir = Path(tmpdir.mkdir("output"))
    mock_work_dir = Path(tmpdir.mkdir("work"))

    mock_gear_context_interface = MagicMock(
        spec=interfaces.ElastixRegGearContextInterface
    )
    logging_mock = mocker.patch("logging.Logger.info")

    with pytest.raises(SystemExit) as e:
        main.run(mock_gear_context_interface, mock_output_dir, mock_work_dir, True)
    assert e.type == SystemExit
    assert e.value.code == 1

    logging_mock.assert_called_with("Exiting....")
