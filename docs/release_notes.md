# Release notes

## 0.1.1_4.9.0

## __Enchancement__:
* Added *transformix* as a new configuration


## 0.1.0

## __Documentation__:
### Elastix-Reg initial release

* Elastix-Reg  is an analysis gear that is based on the Elastix, to register a moving NIfTI image to a target NIfTI image based on to the configuration file.

### Usage

#### Required Inputs:
* __source__ : Required. Source/Moving image to register.
* __target__ : Required. Target/Fixed image to serve as reference
* __elastix_config__ : Required. Parameter file that elastix used to perform registration of the target image. A list of all parameters that can be specified for each registration component can be found at the elastix website: http://elastix.isi.uu.nl/doxygen/parameter.html

#### Configuration
* __debug__ (boolean, default False): Include debug statements in log.
