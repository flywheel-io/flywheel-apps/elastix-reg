#!/usr/bin/env python
"""The run script"""
import logging as log
import os
import sys
from pathlib import Path

from flywheel_gear_toolkit import GearToolkitContext
from nipype import config, logging

from fw_gear_elastix_reg.interfaces import ElastixRegGearContextInterface
from fw_gear_elastix_reg.main import run as elastix_main

cfg = dict(
    execution={
        "stop_on_first_crash": True,
        "hash_method": "content",
        "remove_unnecessary_outputs": False,
        "crashfile_format": "txt",
        "crashdump_dir": os.path.abspath("./output"),
    },
)

config.update_config(cfg)
logging.update_logging(config)
import nipype.pipeline.engine as pe

logger = log.getLogger(__name__)


def main(context: GearToolkitContext):

    # Validate container type
    analysis_id = context.destination["id"]
    analysis = context.client.get_analysis(analysis_id)
    parent_type = analysis.parent.type

    if parent_type != "session":
        logger.error(
            f"This gear must be run at the session container, not {parent_type} level"
        )
        sys.exit(1)

    # Initialize nipype nodes to parse config_json
    context_node = pe.Node(
        ElastixRegGearContextInterface.factory(context.manifest)(),
        name="gear_context_interface",
    )
    context_node.inputs.config_dict = context.config_json

    # Grab work and output dir and other config
    output_folder = context.output_dir
    work_folder = context.work_dir
    transformix_mode = context.config.get("transformix")

    elastix_main(
        context_node,
        Path(output_folder).absolute(),
        Path(work_folder).absolute(),
        transformix_mode,
    )


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        main(gear_context)
