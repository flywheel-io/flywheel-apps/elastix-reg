"""Customized Nipype interfaces"""
import os.path as op

from flywheel_gear_toolkit.interfaces.nipype import GearContextInterfaceBase
from nipype.interfaces.base import File, traits
from nipype.interfaces.elastix.registration import (
    AnalyzeWarp,
    AnalyzeWarpInputSpec,
    AnalyzeWarpOutputSpec,
)

from .parser import generate_inputs_path


class ElastixRegGearContextInterface(GearContextInterfaceBase):
    """Customized Nipype Interface to grab input path from geartoolkit context config_json."""

    def _run_interface(self, runtime):
        self._results = generate_inputs_path(
            self.inputs.config_dict, self._results, self.__class__._manifest
        )

        return runtime


class FWAnalyzeWarpInputSpec(AnalyzeWarpInputSpec):
    """Update existing AnalyzeWarp input spec"""

    jac = traits.Enum(
        "all",
        usedefault=False,
        argstr="-jac %s",
        desc="generate an image with the determinant of the spatial Jacobian",
    )
    jacmat = traits.Enum(
        "all",
        usedefault=False,
        argstr="-jacmat %s",
        desc="generate an image with the spatial Jacobian matrix at each voxel",
    )


class FWAnalyzeWarpOutputSpec(AnalyzeWarpOutputSpec):
    result = File(desc="result")
    transformix_log = File(desc="Transformix log")


class FWAnalyzeWarp(AnalyzeWarp):
    """Elastix Transformix customization."""

    input_spec = FWAnalyzeWarpInputSpec
    output_spec = FWAnalyzeWarpOutputSpec

    def _list_outputs(self):
        outputs = self._outputs().get()
        out_dir = op.abspath(self.inputs.output_path)
        outputs["disp_field"] = op.join(out_dir, "deformationField.nii")
        outputs["result"] = op.join(out_dir, "result.nii")
        outputs["jacdet_map"] = op.join(out_dir, "spatialJacobian.nii")
        outputs["jacmat_map"] = op.join(out_dir, "fullSpatialJacobian.nii")
        outputs["transformix_log"] = op.join(out_dir, "transformix.log")
        return outputs
