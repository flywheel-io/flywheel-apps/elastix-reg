"""Build Elastix Pipeline"""
import logging as log

logger = log.getLogger(__name__)


def split_string(transform_list):

    return transform_list[-1]


def config_elastix_wf(
    work_dir,
    transformix_mode,
):
    """Configure base elastix Nipype workflow

    Args:
        work_dir (pathlib.Path): Path to work directory.
        transformix_mode (bool): If true transformix.exe will be execute.

    Returns:
        nipype.pipeline.engine.workflows.Workflow: Return a nipype Workflow object.
    """
    import nipype.pipeline.engine as pe
    from nipype.interfaces import elastix
    from nipype.interfaces.utility import Function

    from fw_gear_elastix_reg.interfaces import FWAnalyzeWarp

    logger.info("Creating Elastix workflow...")
    elastix_wf = pe.Workflow(name="elastix_registration", base_dir=work_dir)

    reg_node = pe.Node(elastix.Registration(), name="registration")

    # Utility node(s)
    split_output = pe.Node(
        Function(
            input_names=["transform_list"],
            output_names=["transform_parameter"],
            function=split_string,
        ),
        name="split_output",
    )

    try:
        elastix_wf.connect(
            [
                (reg_node, split_output, [("transform", "transform_list")]),
            ]
        )
        if transformix_mode:
            trans_node = pe.Node(FWAnalyzeWarp(), name="transformix")
            elastix_wf.connect(
                [
                    (
                        split_output,
                        trans_node,
                        [("transform_parameter", "transform_file")],
                    )
                ]
            )

    except Exception as err:
        if "TraitError" in str(err.__class__):
            print("TraitError:", err)
        raise
    else:
        return elastix_wf
