"""Main module."""
import logging
import sys

import nipype.pipeline.engine as pe
from nipype.interfaces.io import DataSink
from nipype.interfaces.utility import Rename

from .workflow import config_elastix_wf

logger = logging.getLogger(__name__)


def connect_utility_nodes(
    elastix_workflow, elastix_reg_context_node, output_dir_parents
):
    """Connect other Nipype Nodes to existing workflow.

    Args:
        elastix_workflow (nipype.pipeline.engine.workflows.Workflow): Existing Workflow object
        elastix_reg_context_node (nipype.pe.Node): Nipype Node object for gather config json input path
        output_dir_parents (pathlib.Path): Path to output's parent directory

    Returns:
        nipype.pipeline.engine.workflows.Workflow: Return an updated nipype Workflow object.
    """

    # Base Elastix nodes
    reg_node = elastix_workflow.get_node("registration")
    trans_node = elastix_workflow.get_node("transformix")
    split_output_node = elastix_workflow.get_node("split_output")

    # Utility Nodes
    # Set up sinker nodes here
    logger.info(f"Setting up Datasink")
    sinker_node = pe.Node(DataSink(), name="sinker")
    logger.debug(f"Setting sinker directory to {output_dir_parents}")
    sinker_node.inputs.base_directory = str(output_dir_parents)

    # rename utility
    rename_node = pe.Node(Rename(), name="rename_trans_results")
    rename_node.inputs.format_string = "transformix_result.nii"

    try:
        elastix_workflow.connect(
            [
                (
                    elastix_reg_context_node,
                    reg_node,
                    [
                        ("inputs_source", "moving_image"),
                        ("inputs_target", "fixed_image"),
                        ("inputs_elastix_config", "parameters"),
                    ],
                ),
                (
                    split_output_node,
                    sinker_node,
                    [("transform_parameter", "output.@transformParameter")],
                ),
                (reg_node, sinker_node, [("warped_file", "output.@warped_file")]),
            ]
        )
        if trans_node:
            elastix_workflow.connect(
                [
                    (
                        elastix_reg_context_node,
                        trans_node,
                        [("inputs_source", "moving_image")],
                    ),
                    (trans_node, rename_node, [("result", "in_file")]),
                    (rename_node, sinker_node, [("out_file", "output.@trans_result")]),
                    (
                        trans_node,
                        sinker_node,
                        [
                            ("disp_field", "output.@disp_field"),
                            ("jacdet_map", "output.@jacdet_map"),
                            ("jacmat_map", "output.@jacmat_map"),
                            ("transformix_log", "output.@transformix_log"),
                        ],
                    ),
                ]
            )
    except Exception as err:
        if "TraitError" in str(err.__class__):
            print("TraitError:", err)
        raise

    else:
        return elastix_workflow


def run(context_node, output_folder, work_folder, transformix_mode):
    """Prepare and run Elastix workflow.

    Args:
        context_node (nipype.pe.Node): Nipype Node object for gather config json input path
        output_folder (pathlib.Path): Path to output directory
        work_folder (pathlib.Path): Path to work directory.
        transformix_mode (bool): Transformix mode.
    """

    try:

        # Build elastix nodes and base workflow
        elastix_workflow = config_elastix_wf(work_folder, transformix_mode)

        logger.debug(elastix_workflow.list_node_names())

        elastix_workflow = connect_utility_nodes(
            elastix_workflow, context_node, output_folder.parent.absolute()
        )
        logger.info(elastix_workflow.list_node_names())
        logger.info("Finish workflow build")

        logger.info("Running workflow")
        elastix_workflow.run()
    except Exception:
        logger.exception("Elastix workflow failed...")
        logger.info("Exiting....")
        sys.exit(1)
    else:
        logger.info("Elastix has finished successfully...")
