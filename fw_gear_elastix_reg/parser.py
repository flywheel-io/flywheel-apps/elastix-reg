"""Parser module to parse gear config.json."""
import logging as log

from flywheel_gear_toolkit import GearToolkitContext

logger = log.getLogger(__name__)


def generate_inputs_path(config_dict, results_dict, manifest_dict):
    """Generate input files path for gear run.
    If input file is not provided by user, the system will get the file from the parent/previous session container.

    Args:
        config_dict (dict): Gear Configuration.
        results_dict (dict): Nipype Interface results that is used to generate the interface outputs.
        manifest_dict (dict): Gear Manifest.

    Returns:
        (dict): Return Nipype Interface dictionary.
    """
    gtk_context = GearToolkitContext()
    gtk_context.config_json = config_dict

    config_dict_inputs = config_dict.get("inputs")

    for k, v in manifest_dict["inputs"].items():
        if k not in results_dict and v["base"] == "file":
            tmp_k = "inputs_" + k
            if config_dict_inputs.get(k):
                results_dict[tmp_k] = (
                    config_dict_inputs.get(k).get("location").get("path")
                )
    for k, v in manifest_dict["config"].items():
        if k not in results_dict:
            tmp_k = "config_" + k
            if gtk_context.config.get(k):
                results_dict[tmp_k] = gtk_context.config.get(k)

    return results_dict
