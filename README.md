# Elastix Reg
This gear is based on the Elastix, an image registration toolbox built upon the Insight Segmentation and Registration Toolkit (ITK). The gear will be used to register a moving NIfTI image to a target NIfTI image based on to the configuration file that is provided by the user. This gear will run at the session level as an analysis gear.<br>


## Gear Inputs

### Required
* __source__ : Required. Source/Moving image to register.
* __target__ : Required. Target/Fixed image to serve as reference
* __elastix_config__ : Required. Parameter file that elastix used to perform registration of the target image. A list of all parameters that can be specified for each registration component can be found at the elastix website: http://elastix.isi.uu.nl/doxygen/parameter.html



### Configuration
* __debug__(boolean, default False): Include debug statements in log.
* __transformix__(boolean, default False): If True, the gear will apply a transform on the moving image and/or generates a deformation field.


## Gear outputs
* __result__ : The registered image.
* __transform(s)__ : The affine transform parameter text file that relates the fixed and moving image.
* __deformation__ : The deformation at all voxels of the fixed image.

