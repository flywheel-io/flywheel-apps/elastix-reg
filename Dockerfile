FROM python:3.8-slim as base

# Dev install. git for pip editable install.
RUN apt-get update -qq \
        && apt-get install -y \
        git \
        curl \
        build-essential \
        cmake \
        pkg-config \
        libgdcm-tools \
        libarchive-tools \
        unzip \
        pigz && \
        pip install "poetry==1.0.10"

RUN apt-get update && apt-get install -y elastix=4.9.0-2

ENV FLYWHEEL='/flywheel/v0'
RUN mkdir -p ${FLYWHEEL}
WORKDIR ${FLYWHEEL}

COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev

COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_elastix_reg $FLYWHEEL/fw_gear_elastix_reg
RUN poetry install --no-dev


# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]

